const autoSlider = {
    images: [
        './img/1.jpg',
        './img/2.jpg',
        './img/4.png',
        './img/img.png',
    ],
    interval: null,
    currentSlide: 0,
    imageElem: document.querySelector('.image-to-show'),
    start (){
        this.interval = setInterval(this.change.bind(this),3000)
    },
    change(){

        if ( this.currentSlide <= autoSlider.images.length -1){
            console.log(this.currentSlide);

            this.imageElem.src=this.images[this.currentSlide]
            this.currentSlide++;
        }
        else {
           this.currentSlide = 0;
        }

    },
    stop(){
        clearInterval(this.interval);
    },
};

autoSlider.start();
const stopButton = document.querySelector('.stop');
const resumeButton  = document.querySelector('.resume');
stopButton.addEventListener('click', ev => {
   autoSlider.stop();
})
resumeButton.addEventListener('click', ev => {
    autoSlider.start();
})
