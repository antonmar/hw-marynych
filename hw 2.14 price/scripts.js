
function onFocus() {
    let a = document.getElementById("input");
    a.style.borderColor = "#3CB878";
    a.style.borderWidth = "5px";
    a.style.outline = "none";


}
function onBlur() {
    let a = document.getElementById("input");
    a.style.outline = "2px";
    a.style.borderWidth = "3px";
    if(a.value > 0){
        let body  = document.getElementById("container");
        let subBody = document.createElement("div");
        let b = document.createElement("span");
        let c  = document.createElement("button");
        body.append(subBody);
        subBody.append(b);
        subBody.append(c);
        b.innerHTML = `Current price is     ${document.getElementById("input").value}`;
        subBody.style.marginBottom  = "10px";
        c.className = "button-style";
        c.innerHTML = "X";
        a.style.color = "green";
        subBody.addEventListener("click", function (){
            subBody.removeChild(c);
            subBody.removeChild(b);
            a.value = "";
        })
    }
    else {
        let n = document.getElementById("alert");
        a.style.borderColor = "red";
        n.innerHTML = "Please, enter correct price"
    }



}

