


console.log(localStorage['flag']);
const changeTheme = (flag) => {
  const header = document.querySelector('.header');
 const topMenu  = document.querySelector('.topmenu');
 const sideMenu  = document.querySelectorAll('.side-menu-item');
 const text = document.querySelectorAll('.text-style');
 const footerItems = document.querySelectorAll('.footer-item');
 if(!flag){
     header.classList.add('header-bright');
     topMenu.classList.add('topmenu-bright');
     sideMenu.forEach((e) => {
         e.classList.add('side-menu-item-bright');
     })
     text.forEach((e) => {
         e.classList.add('text-style-bright');
     })
     footerItems.forEach((e) => {
         e.classList.add('footer-item-bright');
     })
 }
 else {
     header.classList.remove('header-bright');
     topMenu.classList.remove('topmenu-bright');
     sideMenu.forEach((e) => {
         e.classList.remove('side-menu-item-bright');
     })
     text.forEach((e) => {
         e.classList.remove('text-style-bright');
     })
     footerItems.forEach((e) => {
         e.classList.remove('footer-item-bright');
     })
 }


}
changeTheme(localStorage['flag']);


const changeButton = document.querySelector('.changer');
changeButton.addEventListener('click', evt => {

    if (!localStorage.getItem('flag')){

        localStorage["flag"] = "true";
    }
    else {

        localStorage['flag'] = "";
    }

    changeTheme(localStorage.getItem('flag'));

})


