
document.addEventListener("DOMContentLoaded", () => { const btn1 = document.getElementById("1");
    const btn2 = document.getElementById("2");
    const btn3= document.getElementById("3");
    const btn4 = document.getElementById("4");
    const btn5 = document.getElementById("5");
    const txt = document.getElementById("txt");
    const img = document.getElementById("img");
    const btn6 = document.getElementById("6");
    const workButtons = document.querySelectorAll('.our-services-button');
    const triangles = document.querySelectorAll('.menu-triangle');
    function clearBackground() {
     workButtons.forEach((element) => {
         element.style.backgroundColor = 'white';
         element.style.color = '#717171'
     })
        triangles.forEach((element) =>{
            element.style.background = "white";
        })
    }
    function changer(n) {
        if(n === 1){
            clearBackground();
            txt.innerHTML = "Products that provide great user experience (e.g., the iPhone) are thus designed with not only the product’s consumption or use in mind but also the entire process of acquiring, owning and even troubleshooting it. Similarly, UX designers don’t just focus on creating products that are usable; " +
                "we concentrate on other aspects of " +
                "the user experience, such as pleasure, " +
                "efficiency and fun, too. able; we concentrate on other asp" +
                "ects of the user experience, such as pleasure, efficiency and fun, too. C" +
                "onsequently, there is no single definition of a good user experience. Inste" +
                "ad, a good user experience is one that meets a" +
                " particular user’s needs in the specific context where he o";
            img.style.backgroundImage = "url('img/graphic-design1.jpg')";
           btn1.style.backgroundColor = " #18CFAB";
           btn1.style.color = "white";
           triangles[0].style.background = "url('img/menu-triangle.png') no-repeat"
        }
        if(n === 2){
            clearBackground();
            txt.innerHTML = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod temp" +
                "or incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proide" +
                "nt, sunt in culpa qui officia deserunt molli" +
                "t anim id est laborum. Lorem ipsum dolor sit amet, " +
                "consectetur adipisicing elit, sed do eiusmod tempor incididunt " +
                "ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non " +
                "proident, sunt in culpa qui officia deserunt mollit anim id est laborum. " +
                "Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur " +
                "adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. " +
                "Excepteur sint occaecat cupidatat non proident, sunt i"

            ;
            img.style.backgroundImage = "url('img/graphic-design2.jpg')";
            btn2.style.backgroundColor = " #18CFAB";
            btn2.style.color = "white";
            triangles[1].style.background = "url('img/menu-triangle.png') no-repeat"
        }
        if(n === 3){
            clearBackground();
            txt.innerHTML = "As a UX designer, you should consider the Why, Wha" +
                "t and How of product use. The Why involves the users’ motivations for adopting a p" +
                "roduct, whether they relate to a task they wish to perform with it or to values and views whi" +
                "ch users associate with the ownership and use of the product. The What addresses the things people" +
                " can do with a product—its functionality. Finally, the How relates to the design of functionality in a" +
                " accessible and aesthetically pleasant way. UX designers start with the Why before determining the What " +
                "and then, fina" +
                "lly, the How in order to create products that users can form meaningful experiences with."

            ;
            img.style.backgroundImage = "url('img/graphic-design3.jpg')";
            btn3.style.backgroundColor = " #18CFAB";
            btn3.style.color = "white";
            triangles[2].style.background = "url('img/menu-triangle.png') no-repeat"
        }
        if(n === 4){
            clearBackground();
            txt.innerHTML = "Since UX design encompasses the entire user " +
                "journey, it’s a multidisciplinary field – UX designers come from a var" +
                "iety of backgrounds such as visual design, programming, psychology and inter" +
                "action design. To design for human users also means you have to work with a heigh" +
                "tened scope regarding accessibility and accommodating many potential users’ physical li" +
                "mitations, such as reading small text. A UX designer’s typical tasks vary, but often include us" +
                "er research, creating personas, designing wireframes and interactive prototypes as well as testing" +
                " designs. These tasks can vary greatly from one organization to the next, but they " +
                "always demand designers to be the users’ advocate and keep the users’ needs at th"

            ;
            img.style.backgroundImage = "url('img/graphic-design4.jpg')";
            btn4.style.backgroundColor = " #18CFAB";
            btn4.style.color = "white";
            triangles[3].style.background = "url('img/menu-triangle.png') no-repeat"
        }
        if(n === 5){
             clearBackground();
            txt.innerHTML = "Throughout this course, you will gain a thoro" +
                "ugh understanding of the various design principles that come toget" +
                "her to create a user’s experience when using a product or service. As yo" +
                "u proceed, you’ll learn the value user experience design brings to a project, " +
                "and what areas you must consider when you want to design great user experiences. B" +
                "ecause user experience is an evolving term, we can’t give you a definition of ‘user e" +
                "xperience’ to end all discussions, but we will provide you with a solid understanding of the" +
                " different aspects of user exper" +
                "ience, so it becomes clear in your mind what is involved in creating great UX designs."

            ;
            img.style.backgroundImage = "url('img/graphic-design5.jpg')";
            btn5.style.backgroundColor = " #18CFAB";
            btn5.style.color = "white";
            triangles[4].style.background = "url('img/menu-triangle.png') no-repeat"
        }
        if(n === 6){
            clearBackground();
            txt.innerHTML = "User experience, or UX, has been a buzzword since about 2005, and according to tech research firm Gartner, the focus on digital experience is no longer limited to digital-born companies anymore. Chances are, you’ve heard of the term, or even have it on your portfolio. But, like most of us, there’s also a good chance that you sometim" +
                "s feel unsure of what the term “user experience” actually covers." +
                +
                "[User experience] is used by people to say, ‘I’m a u" +
                "ser experience designer, I design websites’, or ‘I design apps.’ […] and t" +
                "ey think the experience is that simple device, the website, or the app, or who kn" +
                "ows what. No! It’s everything—it’s the way you experience the world, it’s the way you exp" +
                "erience your life, it’s the way you experience the service. Or, yeah, an app or a computer " +
                "ystem. But it’s a system that’s everything.”"

            ;
            img.style.backgroundImage = "url('img/graphic-design6.jpg')";
            btn6.style.backgroundColor = " #18CFAB";
            btn6.style.color = "white";
            triangles[5].style.background = "url('img/menu-triangle.png') no-repeat"
        }
    }
changer(1);
    btn1.onclick = function(){ changer(1)};
    btn2.onclick = function(){ changer(2)};
    btn3.onclick = function(){ changer(3)};
    btn4.onclick = function(){ changer(4)};
    btn5.onclick = function(){ changer(5)};
    btn6.onclick = function(){ changer(6)};
})

const images = [
    {
        src: 'img' + '/' + 'landingPage' + '/' + 'landing-page1.jpg',
        category: 'landingPage'
    },    {
        src: 'img/landingPage/landing-page2.jpg',
        category: 'landingPage'
    },    {
        src: 'img/landingPage/landing-page3.jpg',
        category: 'landingPage'
    },    {
        src: 'img/landingPage/landing-page4.jpg',
        category: 'landingPage'
    },    {
        src: 'img/landingPage/landing-page5.jpg',
        category: 'landingPage'
    },    {
        src: 'img/landingPage/landing-page6.jpg',
        category: 'landingPage'
    },    {
        src: 'img/landingPage/landing-page7.jpg',
        category: 'landingPage'
    },    {
        src: 'img/graphicDesign/graphic-design1.jpg',
        category: 'graphicDesign'
    },    {
        src: 'img/graphicDesign/graphic-design2.jpg',
        category: 'graphicDesign'
    },{
        src: 'img/graphicDesign/graphic-design3.jpg',
        category: 'graphicDesign'
    },{
        src: 'img/graphicDesign/graphic-design4.jpg',
        category: 'graphicDesign'
    },{
        src: 'img/graphicDesign/graphic-design5.jpg',
        category: 'graphicDesign'
    },{
        src: 'img/graphicDesign/graphic-design6.jpg',
        category: 'graphicDesign'
    },{
        src: 'img/graphicDesign/graphic-design7.jpg',
        category: 'graphicDesign'
    },{
        src: 'img/graphicDesign/graphic-design8.jpg',
        category: 'graphicDesign'
    },{
        src: 'img/graphicDesign/graphic-design9.jpg',
        category: 'graphicDesign'
    },{
        src: 'img/graphicDesign/graphic-design10.jpg',
        category: 'graphicDesign'
    },{
        src: 'img/graphicDesign/graphic-design11.jpg',
        category: 'graphicDesign'
    },{
        src: 'img/graphicDesign/graphic-design12.jpg',
        category: 'graphicDesign'
    },{
        src: 'img/wordpress/wordpress1.jpg',
        category: 'wordpress'
    },{
        src: 'img/wordpress/wordpress2.jpg',
        category: 'wordpress'
    },{
        src: 'img/wordpress/wordpress3.jpg',
        category: 'wordpress'
    },{
        src: 'img/wordpress/wordpress4.jpg',
        category: 'wordpress'
    },
    {
        src: 'img/wordpress/wordpress5.jpg',
        category: 'wordpress'
    },{
        src: 'img/wordpress/wordpress6.jpg',
        category: 'wordpress'
    },{
        src: 'img/wordpress/wordpress7.jpg',
        category: 'wordpress'
    },{
        src: 'img/wordpress/wordpress8.jpg',
        category: 'wordpress'
    },{
        src: 'img/wordpress/wordpress9.jpg',
        category: 'wordpress'
    },{
        src: 'img/wordpress/wordpress10.jpg',
        category: 'wordpress'
    },{
        src: 'img/webDesign/web-design1.jpg',
        category: 'webDesign'
    },{
        src: 'img/webDesign/web-design2.jpg',
        category: 'webDesign'
    },{
        src: 'img/webDesign/web-design3.jpg',
        category: 'webDesign'
    },{
        src: 'img/webDesign/web-design4.jpg',
        category: 'webDesign'
    },{
        src: 'img/webDesign/web-design5.jpg',
        category: 'webDesign'
    },{
        src: 'img/webDesign/web-design6.jpg',
        category: 'webDesign'
    },{
        src: 'img/webDesign/web-design7.jpg',
        category: 'webDesign'
    },
]
const allButtons = document.querySelectorAll('.our-work-button');
allButtons[0].style.color = '#18CFAB';
allButtons[0].style.border = ' 2px solid #18CFAB';
for (let i =1; i<allButtons.length; i++){
    allButtons[i].addEventListener('click', evt => {
        allButtons[0].style.color = '#717171';
        allButtons[0].style.border = ' 1px solid #DADADA';
    })
}
allButtons[0].addEventListener('click', evt => {
    allButtons[0].style.color = '#18CFAB';
    allButtons[0].style.border = ' 2px solid #18CFAB';
})
let perPage = 12;
let currentCategory=`all`;
const render = (arr) => {
    const mainContainer = document.querySelector('.our-work-images');
    const sliceArray = arr.slice(0, perPage);
    const htmlArray = sliceArray.map((element) => {
        return `
<div class="our-work-images-wrapper" >
<div class="hidden-block" style="background-image: url('${element.src}')">
 <div class="hidden-hover-part" >
 <div class="two-circles">
 <div class="first-circle">
 <div class="combined-shape">
 
</div>
</div>
<div class="second-circle">
 <div class="white-square">
 
</div>
</div>
</div>
<div class="hidden-text-1">
CREATIVE DESIGN
</div>
<div class="hidden-text-2">
Web Design
</div>
</div>
</div>
</div>
`

    });

    htmlArray.forEach(e => {
        mainContainer.innerHTML = e;
    })
    mainContainer.innerHTML = htmlArray.join(' ');
    const btn = document.querySelector('.our-work-btn');

    if (perPage >= arr.length) {
        btn.classList.add(`hidden`);
    } else {
        btn.classList.remove(`hidden`);
    }
}
render(images);
const filteredArr = (arr, category) => {
    return arr.filter((e)=> e.category === category)
}
console.log(filteredArr(images))

const tabsContainer = document.querySelector('.our-work-buttons');
tabsContainer.addEventListener('click', (event) => {
    const category = event.target.dataset.category;
    currentCategory = category;
    perPage = 12;
    if (category === "all" && event.target !== event.currentTarget) {
        render(images);
    } else if (event.target !== event.currentTarget) {
        const newArr = filteredArr(images, category);
        render(newArr);}
    const tabs = document.querySelectorAll('.tab');
    tabs.forEach(e => {
        e.classList.remove('active');
    });
    event.target.classList.add('active');
});

const button = document.querySelector(".our-work-btn");
button.addEventListener(`click`, () => {

        perPage = perPage+12;


    if(currentCategory===`all`){
        render(images);
    }else{
        render(filteredArr (images, currentCategory));
    }
})
const peopleSay = [
    {
      name: "Eva Lightner",
      text: "We love the ham cuz it tastes like hamburger",
      prof: "weird woman",
      img: "img/face1.png" ,
    },
    {
      name: "Adam Blishchinsky",
      text: "The ham is just ridiculously bad",
      prof: 'hater',
        img: "img/face2.png" ,
    },
    {
        name: "Monkey D Luffy",
        text: 'I am gonna become the king of pirates',
        prof: 'main character ',
        img: "img/face3.png" ,
    },
    {
        name: "Asian Woman",
        text: "Hello, I am Asian woman! Nice to meet you",
        prof: "journalist",
        img: "img/face4.png" ,
    }


]

let currentSituation = 0;
let currentTop = 0;
const textContainer = document.querySelector('.people-say-text');
const nameContainer = document.querySelector('.people-say-name');
const profContainer = document.querySelector('.people-say-prof');
const faces = document.querySelectorAll('.slider-element');
const bigFace = document.querySelector('.people-say-big-img');
const leftButton = document.getElementById('sBtn1');
const rightButton = document.getElementById('sBtn2');
const showFaces = () => {
    let peopleSayCounter = 0;
   textContainer.innerHTML = peopleSay[currentTop].text;
    nameContainer.innerHTML = peopleSay[currentTop].name;
    profContainer.innerHTML = peopleSay[currentTop].prof;
    for(let i = 0; i<faces.length; i++) {
        faces[i].addEventListener("click", evt => {
            currentTop = i;
            clearHigherPosition();
            bigFace.classList.remove(bigFace.classList[1]);
            faces[i].classList.add('slider-higher-position');
            bigFace.classList.add(faces[i].classList[1]);
            textContainer.innerHTML = peopleSay[currentTop].text;
            nameContainer.innerHTML = peopleSay[currentTop].name;
            profContainer.innerHTML = peopleSay[currentTop].prof;

        })
       faces[i].classList.remove("people-say-img-" + String(((currentSituation+2+(peopleSayCounter%4)))%4))
        faces[i].classList.remove("people-say-img-" + String(((currentSituation+(peopleSayCounter%4)))%4))
        faces[i].classList.add("people-say-img-" + String(((currentSituation+1+(peopleSayCounter%4)))%4));
        peopleSayCounter++;

    }

}
showFaces();

rightButton.addEventListener('click', ev => {
    clearHigherPosition();
    console.log(currentTop)
    bigFace.classList.remove("people-say-img-" + String(currentTop+1));
    currentTop++;
    if (currentTop>3){
        currentTop = 0;
    }
    showFaces();
    faces[currentTop].classList.add('slider-higher-position');
    setBigImg();
    console.log(currentTop);
    console.log(bigFace)
})
leftButton.addEventListener('click', ev => {
    clearHigherPosition();
    bigFace.classList.remove("people-say-img-" + String(currentTop+1));
    currentTop--;
    if (currentTop<0){
        currentTop = 3;
    }
    faces[currentTop].classList.add('slider-higher-position')




showFaces();
    setBigImg();
    console.log(currentTop)
})

function clearHigherPosition() {
    faces.forEach((el) => {
        el.classList.remove('slider-higher-position')
    })
}
function setBigImg() {
    for (let i = 0; i<faces.length; i++){
        if(faces[i].classList.contains('slider-higher-position')){
            bigFace.classList.add("people-say-img-" + String((currentTop+1)%4))
        }
    }
}



